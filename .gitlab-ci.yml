workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never
    - when: always

stages:
  - test
  - build
  - deploy_dev
  - deploy_staging
  - deploy_prod

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE

run_unit_tests:
  image: node:18-alpine
  stage: test
  tags:
    - aws
    - docker
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      - app/node_modules
  before_script:
    - cd app
    - npm install
  script:
    - npm test
  artifacts:
    when: always
    paths:
      - app/junit.xml
    reports:
      junit: app/junit.xml

sast:
  stage: test

build_image:
  stage: build
  tags:
    - aws
    - shell
  before_script:
    - export APP_VERSION=$(cat app/package.json | jq -r .version)
    - export VERSION=$APP_VERSION.$CI_PIPELINE_IID
    - echo "VERSION=$VERSION" >> build.env
  script:
    - docker build -t $IMAGE_NAME:$VERSION .
  artifacts:
    reports:
      dotenv:
        - build.env

push_image:
  stage: build
  tags:
    - aws
    - shell
  needs:
    - build_image
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker push $IMAGE_NAME:$VERSION

.deploy:
  tags:
    - aws
    - shell
  variables:
    SSH_KEY: ""
    SERVER_HOST: ""
    DEPLOY_ENV: ""
    APP_PORT: ""
    ENDPOINT: ""
  before_script:
    - echo $SSH_KEY | sed -e "s/-----BEGIN RSA PRIVATE KEY-----/&\n/" -e "s/-----END RSA PRIVATE KEY-----/\n&/" -e "s/\S\{64\}/&\n/g"\ > deploy-key.pem
    - chmod 400 deploy-key.pem
  script:
    - scp -o StrictHostKeyChecking=no -i deploy-key.pem ./docker-compose.yml ubuntu@$SERVER_HOST:/home/ubuntu
    - ssh -o StrictHostKeyChecking=no -i deploy-key.pem ubuntu@$SERVER_HOST "
      docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY &&
      
      export COMPOSE_PROJECT_NAME=$DEPLOY_ENV &&
      export DC_IMAGE_NAME=$IMAGE_NAME &&
      export DC_IMAGE_TAG=$VERSION &&
      export DC_APP_PORT=$APP_PORT &&
      
      docker-compose down &&
      docker-compose up -d"
  environment:
    name: $DEPLOY_ENV
    url: $ENDPOINT

deploy_to_dev:
  extends: .deploy
  stage: deploy_dev
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $DEV_SERVER_HOST
    DEPLOY_ENV: development
    APP_PORT: 3000
    ENDPOINT: $DEV_ENDPOINT

run_functional_tests:
  stage: deploy_dev
  needs:
    - deploy_to_dev
  script:
    - echo "Running functional tests..."

deploy_to_staging:
  extends: .deploy
  stage: deploy_staging
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $STAGING_SERVER_HOST
    DEPLOY_ENV: staging
    APP_PORT: 4000
    ENDPOINT: $STAGING_ENDPOINT

run_performance_tests:
  stage: deploy_staging
  needs:
    - deploy_to_staging
  script:
    - echo "Running performance tests..."

deploy_to_prod:
  extends: .deploy
  stage: deploy_prod
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $PROD_SERVER_HOST
    DEPLOY_ENV: prod
    APP_PORT: 5000
    ENDPOINT: $PROD_ENDPOINT
  when: manual

include:
  - template: Security/SAST.gitlab-ci.yml
